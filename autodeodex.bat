@ECHO OFF

SET DEODEX_PRE=deodexed_
SET APP_DIR=app
SET FRAMEWORK_DIR=framework
SET TOOLS_DIR=tools
SET SMALI=smali-1.3.3.jar
SET BAKSMALI=baksmali-1.3.3.jar
SET BOOTCLASSPATH=:core.jar:ext.jar:framework.jar:android.policy.jar:services.jar
SET COMPRESSIONLEVEL=9
SET DEODEX=0
SET IGNORE=0
SET SIGN=0
SET ZIPALIGN=0
SET SDK_VERSION=13

ECHO [+] AutoDeodex v1.3 (worstenbrood@gmail.com)
ECHO --------------------------------------------
ECHO.

:LOOP
IF "%1"=="" GOTO CONTINUE
IF "%1"=="/i" (
	SET IGNORE=1
	GOTO NEXT
)
IF "%1"=="/d" (
	SET DEODEX=1
	GOTO NEXT
)
IF "%1"=="/s" (
	SET SIGN=1
	GOTO NEXT
)
IF "%1"=="/z" (
	SET ZIPALIGN=1
	GOTO NEXT
)
IF "%1"=="/b" (
	IF NOT "%2"=="" (
		SET BOOTCLASSPATH=%BOOTCLASSPATH%:%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/c" (
	IF NOT "%2"=="" (
		SET COMPRESSIONLEVEL=%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/v" (
	IF NOT "%2"=="" (
		SET SDK_VERSION=%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/?" (
	ECHO %~n0%~x0 [/d] [/i] [/s] [/z] [/b class] [/c compressionlevel] [/v SDK_VERSION]
	ECHO.
	ECHO /d: Deodex files
	ECHO /i: Ignore baksmali errors
	ECHO /s: Sign APK files with testkey
	ECHO /z: Zipalign APK/JAR files
	ECHO /b class: Add class to bootclasspath (Default: %BOOTCLASSPATH%)
	ECHO /c compressionlevel: Set APK/JAR compression level (Default: %COMPRESSIONLEVEL%)
	ECHO /v SDK_VERSION: Set SDK version of rom  (Default: %SDK_VERSION%)
	GOTO QUIT
)
:NEXT
SHIFT /1
GOTO LOOP
:CONTINUE

ECHO [+] Checking java ...
java -version 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" (
	ECHO [-] Java not found !
	GOTO QUIT
)

ECHO [+] Building bootclass path ...
FOR %%F IN (%~dp0%FRAMEWORK_DIR%\*.jar) DO CALL buildbootclasspath.bat %%F
FOR %%F IN (%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%\*.jar) DO CALL buildbootclasspath.bat %%F
IF "%BOOTCLASSPATH%"=="" (
	ECHO [-] BootClassPath is empty !
	GOTO QUIT
)
ECHO [+] BootClassPath: %BOOTCLASSPATH%

IF "%DEODEX%"=="1" (
	ECHO [+] Checking directories ...
	FOR %%D IN ("%~dp0%APP_DIR%","%~dp0%FRAMEWORK_DIR%") DO (
		IF NOT EXIST "%%D\." (
			ECHO [-] Directory %%D not found !
			GOTO QUIT
		)
	)

	ECHO [+] Deodexing JAR files ...
	FOR %%F IN (%~dp0%FRAMEWORK_DIR%\*.jar) DO CALL deodex.bat %%F %FRAMEWORK_DIR%
	FOR %%F IN (%~dp0%FRAMEWORK_DIR%\*.apk) DO CALL move.bat %%F %~dp0%DEODEX_PRE%%FRAMEWORK_DIR%

	ECHO [+] Deodexing APK files ...
	FOR %%F IN (%~dp0%APP_DIR%\*.apk) DO CALL deodex.bat %%F %APP_DIR%
)

IF "%SIGN%"=="1" (
	ECHO [+] Signing APK files ...
	FOR %%F IN (%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%\*.apk) DO CALL sign.bat %%F
	FOR %%F IN (%~dp0%DEODEX_PRE%%APP_DIR%\*.apk) DO CALL sign.bat %%F
)

IF "%ZIPALIGN%"=="1" (
	ECHO [+] Zipaligning files ...
	FOR %%F IN (%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%\*.jar) DO CALL align.bat %%F
	FOR %%F IN (%~dp0%DEODEX_PRE%%FRAMEWORK_DIR%\*.apk) DO CALL align.bat %%F
	FOR %%F IN (%~dp0%DEODEX_PRE%%APP_DIR%\*.apk) DO CALL align.bat %%F
)

:QUIT
SET DEODEX_PRE=
SET APP_DIR=
SET FRAMEWORK_DIR=
SET TOOLS_DIR=
SET SMALI=
SET BAKSMALI=
SET BOOTCLASSPATH=
SET COMPRESSIONLEVEL=
SET DEODEX=
SET IGNORE=
SET SIGN=
SET ZIPALIGN=